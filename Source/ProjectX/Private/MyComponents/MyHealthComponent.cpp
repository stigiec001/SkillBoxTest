// Fill out your copyright notice in the Description page of Project Settings.


#include "MyComponents/MyHealthComponent.h"

// Sets default values for this component's properties
UMyHealthComponent::UMyHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...

	CurrentHealth = MaxHealth;

}


// Called when the game starts
void UMyHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


// Called every frame
void UMyHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UMyHealthComponent::RestoretHealth(float value)
{
	CurrentHealth = FMath::Clamp(CurrentHealth + value, 0.f, MaxHealth);
	OnHealthChange.Broadcast(CurrentHealth);
}

void UMyHealthComponent::ReceiveDamage(float value)
{
	CurrentHealth = FMath::Clamp(CurrentHealth - value, 0.f, MaxHealth);
	if (CurrentHealth <= 0.f)
	{
		OnDeath.Broadcast();
		DeathEvent();
	}
	OnHealthChange.Broadcast(CurrentHealth);
}

void UMyHealthComponent::DeathEvent_Implementation()
{
}

