// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyComponents/MyHealthComponent.h"
#include "PlayerHealthComponent.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTX_API UPlayerHealthComponent : public UMyHealthComponent
{
	GENERATED_BODY()
	
};
