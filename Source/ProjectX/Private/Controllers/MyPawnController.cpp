// Fill out your copyright notice in the Description page of Project Settings.


#include "Controllers/MyPawnController.h"
#include "Pawns/MyPawn.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"

AMyPawnController::AMyPawnController()
{

}

void AMyPawnController::BeginPlay()
{

}

void AMyPawnController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	if (GetPawn())
	{
		RotateToCursor();
		if (!GetPawn()->GetPendingMovementInputVector().IsZero())
		{
			GetPawn()->GetMovementComponent()->AddInputVector(GetPawn()->ConsumeMovementInputVector());
			//GetPawn()->SetActorLocation(GetPawn()->GetActorLocation() + GetPawn()->ConsumeMovementInputVector(), true);
		}
	}

}

void AMyPawnController::SetupInputComponent()
{
	Super::SetupInputComponent();

	//Bind Actions
	InputComponent->BindAction("Fire", IE_Pressed, this, &AMyPawnController::OnShootPressed);
	InputComponent->BindAction("Fire", IE_Released, this, &AMyPawnController::OnShootReleased);

	// Bind Axis
	InputComponent->BindAxis("MoveForward", this, &AMyPawnController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AMyPawnController::MoveRight);
}

void AMyPawnController::RotateToCursor()
{
	if (AMyPawn* MyPawn = Cast<AMyPawn>(GetPawn()))
	{
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);
		float Yaw = UKismetMathLibrary::FindLookAtRotation(MyPawn->GetActorLocation(), Hit.Location).Yaw;
		MyPawn->SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}

void AMyPawnController::MoveForward(float Value)
{
	if (AMyPawn* MyPawn = Cast<AMyPawn>(GetPawn()))
	{
		UE_LOG(LogTemp, Log, TEXT("X: %f"), Value);
		MyPawn->AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Value * MyPawn->MoveSpeed);
	}
}

void AMyPawnController::MoveRight(float Value)
{
	if (AMyPawn* MyPawn = Cast<AMyPawn>(GetPawn()))
	{
		UE_LOG(LogTemp, Log, TEXT("Y: %f"), Value);
		MyPawn->AddMovementInput(FVector(0.0f, 1.0f, 0.0f), Value * MyPawn->MoveSpeed);
	}
}

void AMyPawnController::OnShootPressed()
{
	if (AMyPawn* MyPawn = Cast<AMyPawn>(GetPawn()))
	{
		MyPawn->BeginShoot();
	}
}

void AMyPawnController::OnShootReleased()
{
	if (AMyPawn* MyPawn = Cast<AMyPawn>(GetPawn()))
	{
		MyPawn->StopShoot();
	}
}
