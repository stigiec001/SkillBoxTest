// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MyPawnController.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTX_API AMyPawnController : public APlayerController
{
	GENERATED_BODY()

public:
	AMyPawnController();

	UFUNCTION()
		void BeginPlay() override;

protected:

	// Player Tick function.
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// Rotate Pawn to cursor position.
	void RotateToCursor();

	// Move controlls
	void MoveForward(float Value);
	void MoveRight(float Value);

	// Fire Script
	void OnShootPressed();
	void OnShootReleased();
};
