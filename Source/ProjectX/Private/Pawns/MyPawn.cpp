// Fill out your copyright notice in the Description page of Project Settings.

#include "Camera/CameraComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Pawns/MyPawn.h"

// Sets default values
AMyPawn::AMyPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create Mesh
	PawnMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PawnMesh"));
	PawnMeshComponent->SetupAttachment(RootComponent);
	
	// Create Spring Arm
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComponent->SetupAttachment(PawnMeshComponent);
	SpringArmComponent->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when ship does
	SpringArmComponent->TargetArmLength = 1200.f;
	SpringArmComponent->SetRelativeRotation(FRotator(-90.f, 0.f, 0.f));
	SpringArmComponent->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	//Create Camera 
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	CameraComponent->SetupAttachment(SpringArmComponent);
	CameraComponent->bUsePawnControlRotation = false;

	//Create Collision Sphere
	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->SetupAttachment(RootComponent);
	CollisionSphere->SetSphereRadius(32);

	//Create FireRoot
	FireRoot = CreateDefaultSubobject<USphereComponent>(TEXT("FireRoot"));
	FireRoot->SetupAttachment(PawnMeshComponent);
	FireRoot->SetSphereRadius(16);

}

// Called when the game starts or when spawned
void AMyPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsShooting)
	{
		Fire();
	}

}

// Called to fire projectile
void AMyPawn::Fire()
{
	if (bCanFire)
	{
		if (FireRoot)
		{

			const FRotator SpawnRotation = GetActorRotation();
			// Spawn projectile at an offset from this pawn
			const FVector SpawnLocation = FireRoot->GetComponentLocation();

			UWorld* const World = GetWorld();
			if (World != nullptr)
			{
				FActorSpawnParameters SpawnParam;
				SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParam.Owner = GetOwner();
				SpawnParam.Instigator = GetInstigator();
				// spawn the projectile
				World->SpawnActor(Projectile, &SpawnLocation, &SpawnRotation, SpawnParam);
			}

			bCanFire = false;
			World->GetTimerManager().SetTimer(TimerHandle_ShotTimerExpired, this, &AMyPawn::ShotTimerExpired, FireRate);

			// try and play the sound if specified
			if (FireSound != nullptr)
			{
				UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
			}

			bCanFire = false;
		}
	}
}

void AMyPawn::ShotTimerExpired()
{
	bCanFire = true;
}
