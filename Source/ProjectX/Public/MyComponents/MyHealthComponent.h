// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MyHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHealthChange, float, Health);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);

USTRUCT(BlueprintType)
struct FStatsParam
{
	GENERATED_BODY()
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROJECTX_API UMyHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UMyHealthComponent();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnHealthChange OnHealthChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnDeath OnDeath;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	float CurrentHealth = 0.f;


public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float MaxHealth = 100.f;
	UFUNCTION(BlueprintCallable, Category = "Health")
	float GetCurrentHealth() { return CurrentHealth; };
	UFUNCTION(BlueprintCallable, Category = "Health")
	void RestoretHealth(float value);
	UFUNCTION(BlueprintCallable, Category = "Health")
	void ReceiveDamage(float value);
	UFUNCTION(BlueprintNativeEvent)
	void DeathEvent();

};
